const fs = require('fs')

function getResult1(data) {
  // read 3 lines at a time
  // find any simbol different from /\.|[0-9]/
  // check every character around the symbole and search for /[0-9]: 
  //  - line[symbolLine][line.indexOf(symbolLine) - 1]
  //  - line[symbolLine][line.indexOf(symbolLine) + 1]
  //  - line[symbolLine - 1][line.indexOf(symbolLine)]
  //  - line[symbolLine - 1][line.indexOf(symbolLine) - 1]
  //  - line[symbolLine - 1][line.indexOf(symbolLine) + 1]
  //  - line[symbolLine + 1][line.indexOf(symbolLine)]
  //  - line[symbolLine + 1][line.indexOf(symbolLine) - 1]
  //  - line[symbolLine + 1][line.indexOf(symbolLine) + 1]
  
  
  // ....752
  // ....752*
  // ....*752
  
  // ..752......

  const numbers = []
  for (let lineNumber = 0; lineNumber < data.length; lineNumber++) {
    const symbols = [...data[lineNumber].matchAll(/(?!(\.|[0-9]))./gi)]
    console.log('symbols', symbols)
    symbols.forEach(s => {
      console.log('index', s.index)
      console.log('char', data[lineNumber][s.index])

      if (lineNumber !== data.length - 1) { // not last line
        // checking previous line
      }

      if (lineNumber !== 0) { // not first line
        // checking next line
      }

      // checking current line
      console.log('checking current line')
      console.log('checking previous char')
      if (!isNaN(Number(data[lineNumber][s.index - 1]))) {
        let n = data[lineNumber].slice(s.index - 3, s.index)
        n = n.replace(/(?!([0-9]))./, '')
        const replacement = Array.from(n, c => '.').join('')

        data[lineNumber] = data[lineNumber].substring(0, s.index - n.length) + replacement + data[lineNumber].substring(s.index, data[lineNumber].length)

        n = Number(n)
        numbers.push(n)
      }
      
      console.log('checking next char')
      if (!isNaN(Number(data[lineNumber][s.index + 1]))) {
        console.log('FOUND ! ', data[lineNumber][s.index + 1])
        console.log(`number : '${data[lineNumber].slice(s.index, s.index + 4)}'`)

        let n = data[lineNumber].slice(s.index, s.index + 4)
        n = n.replace(/(?!([0-9]))./, '')
        const replacement = Array.from(n, c => '.').join('')

        data[lineNumber] = data[lineNumber].substring(0, s.index) + replacement + data[lineNumber].substring(s.index + n.length, data[lineNumber].length)

        n = Number(n)
        numbers.push(n)
      } else {
        console.log('NOT FOUND ! ', data[lineNumber][s.index + 1])
      }

      console.log('-----------------')
    })
  }
  console.log(numbers)

  // console.log('coordinates', coordinates)
}

function getResult2(data) {

}

function main() {
  fs.readFile('./_input', 'utf8', (err, data) => {
    if (err) {
      console.error(err)
      return
    }

    const result1 = getResult1(data.split('\n'))
    const result2 = getResult2(data.split('\n'))

    console.log(`result phase 1 : ${result1}`)
    console.log(`result phase 2 : ${result2}`)
  })
}

main()