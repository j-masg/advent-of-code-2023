const fs = require('fs')

function getResult1(data) {
  const MAX = {blue: 14, green: 13, red: 12}
  const possibleGames = []
  const impossibleGames = []
  
  for (const line of data) {
    let [gameId, gameResult] = line.split(': ')
    gameId = gameId.split(' ')[1]

    if (!gameId || !gameResult) continue

    const sets = gameResult.split('; ')
    let impossibleGame = false

    for (const set of sets) {
      const c = set.split(', ')
      const colors = {}
      c.forEach(e => {
        colors[e.split(' ')[1]] = Number(e.split(' ')[0])
      })

      if (
        colors['blue'] > MAX['blue'] 
        || colors['green'] > MAX['green'] 
        || colors['red'] > MAX['red']
      ) {
        impossibleGame = true
      }
    }

    if (impossibleGame) {
      impossibleGames.push(Number(gameId))
    } else {
      possibleGames.push(Number(gameId))
    }
  }

  console.log(possibleGames)
  return possibleGames.reduce((acc, curr) => acc + curr, 0)
}

function getResult2(data) {
  const powers = []

  for (const line of data) {
    let [gameId, gameResult] = line.split(': ')
    gameId = gameId.split(' ')[1]

    if (!gameId || !gameResult) continue

    const sets = gameResult.split('; ')

    const setColors = {blue: [], red: [], green: []}
    for (const set of sets) {
      const c = set.split(', ')
      const colors = {}
      c.forEach(e => {
        colors[e.split(' ')[1]] = Number(e.split(' ')[0])
        setColors[e.split(' ')[1]].push(Number(e.split(' ')[0]))
      })

      console.log('colors', colors)
    }
    console.log('setColors', setColors)
    const maxColors = {blue: 0, red: 0, green: 0}
    for (const [key, values] of Object.entries(setColors)) {
      console.log(`${key}: ${values}`)
      maxColors[key] = Math.max(...values)
    }

    console.log('maxColors', maxColors)
    const power = maxColors['blue'] * maxColors['red'] * maxColors['green']
    console.log('power', power)
    powers.push(power)
  }

  return powers.reduce((acc, curr) => acc + curr, 0)
}

function main() {
  fs.readFile('./_input', 'utf8', (err, data) => {
    if (err) {
      console.error(err)
      return
    }

    const result1 = getResult1(data.split('\n'))
    const result2 = getResult2(data.split('\n'))

    console.log(`result phase 1 ${result1}`)
    console.log(`result phase 2 ${result2}`)
  })
}

main()