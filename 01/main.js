const fs = require('fs')

function getResult1(data) {
  const numbers = []
  for (const line of data) {
    const num = line.match(/[0-9]/gi)
    if (num) {
      numbers.push(Number(`${num[0]}${num[num.length - 1]}`))
    }
  }
  return numbers.reduce((acc, curr) => acc + curr, 0)
}

function getResult2(data) {
  const numbers = []
  const numDict = {
    one: 1, 
    two: 2, 
    three: 3, 
    four: 4, 
    five: 5, 
    six: 6, 
    seven: 7, 
    eight: 8, 
    nine: 9
  }
  for (const line of data) {
    let subLine = line
    const num = []
    while (subLine !== '') {
      let n = subLine.match(/[0-9]|one|two|three|four|five|six|seven|eight|nine/i)
      n = n ? n[0] : null
      if (n && isNaN(Number(n))) {
        num.push(numDict[n])
        subLine = subLine.slice(subLine.indexOf(n))
        subLine = subLine.slice(subLine.indexOf(n[n.length - 1]))
      } else if (n && !isNaN(Number(n))) {
        num.push(Number(n))
        subLine = subLine.slice(subLine.indexOf(n) + 1)
      } else {
        subLine = ''
      }
    }
    
    if (num && num.length > 0) {
      numbers.push(Number(`${num[0]}${num[num.length - 1]}`))
    }
  }

  return numbers.reduce((acc, curr) => acc + curr, 0)
}

function main() {
  fs.readFile('./_input', 'utf8', (err, data) => {
    if (err) {
      console.error(err)
      return
    }

    const result1 = getResult1(data.split('\n'))
    const result2 = getResult2(data.split('\n'))

    console.log(`result phase 1 : ${result1}`)
    console.log(`result phase 2 : ${result2}`)
  })
}

main()